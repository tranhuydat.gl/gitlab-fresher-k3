import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, Link,NavLink} from 'react-router-dom'
import ListProductComponents from './components/ListProductComponents';
import HeaderComponents from './components/HeaderComponents';
import FooterComponents from './components/FooterComponents';
import CreateProductComponents from './components/CreateProductComponents';
import UpdateProductComponents from './components/UpdateProductComponents';

function App() {
  return ( 
      <Router>
        <HeaderComponents/>  
          <div className= "container">
            <Route path= "/product" exact component = {ListProductComponents} /> 
            <Route path= "/addProduct" exact component = {CreateProductComponents} />   
            <Route path= "/updateProduct/:id" exact component = {UpdateProductComponents} />
          </div>
        <FooterComponents/>
      </Router>
  );
}

export default App;
