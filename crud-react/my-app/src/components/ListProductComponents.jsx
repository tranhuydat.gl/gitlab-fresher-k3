import React, { Component } from 'react';
import ProductServices from '../services/ProductServices';

class ListProductComponents extends Component {
    constructor(props){
        super(props)

        this.state = {
            product: []
        }
        this.addProduct = this.addProduct.bind(this);
        this.editProduct = this.editProduct.bind(this);
    }

    componentDidMount(){
        ProductServices.getProduct().then((res) =>{
            this.setState({product: res.data})
        });
    }

    addProduct(){
        this.props.history.push('/addProduct')
    }

    editProduct(id){
        this.props.history.push(`/updateProduct/${id}`)
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Product List</h2>
                <div className="add-product">
                    <button className="btn-btn" onClick={this.addProduct}>Add Product</button>
                </div>
                <div className="row">
                    <table className= "table table-stiped table-bodered">
                        <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Product Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.product.map(
                                    produc=>
                                        <tr key={produc.id}>
                                            <td>{produc.name}</td>
                                            <td>{produc.price}</td>
                                            <td>
                                                <button onClick={() => this.editProduct(produc.id)} className = "btn btn-info">Update</button>
                                            </td>
                                        </tr>
                                    )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListProductComponents;