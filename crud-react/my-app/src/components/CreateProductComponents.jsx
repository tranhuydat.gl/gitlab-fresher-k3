import React, { Component } from 'react';
import ProductServices from '../services/ProductServices';

class CreateProductComponents extends Component {
    constructor(props){
        super(props)

        this.state = {
            name: '',
            price: ''
        }

        this.changeProductNameHandler = this.changeProductNameHandler.bind(this)
        this.changeProductPriceHandler = this.changeProductPriceHandler.bind(this)
        this.saveProduct = this.saveProduct.bind(this)
    }
    changeProductNameHandler= (event) =>{
        this.setState({name: event.target.value})
    }

    changeProductPriceHandler=(event) =>{
        this.setState({price: event.target.value})
    }

    saveProduct = (p) => {
        p.preventDefault()
        let product = {name: this.state.name, price: this.state.price}
        console.log('product =>' +JSON.stringify(product))

        ProductServices.createProduct(product).then(res=>{
            this.props.history.push('/product')
        })
    }

    cancel(){
        this.props.history.push('/product')
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="h3">Add Product</div>
                        <div className="add-form">
                            <form>
                                <label>Product Name: </label>
                                <input value={this.state.name} name="name" onChange = {this.changeProductNameHandler}/>
                                <label>Product Price: </label>
                                <input value={this.state.price} name="price" onChange = {this.changeProductPriceHandler}/>
                                <button onClick={this.saveProduct}>Save</button>
                                <button onClick={this.cancel.bind(this)}>Cancel</button>
                            </form>
                        </div>
                    </div>    
                </div>
            </div>
        );
    }
}

export default CreateProductComponents;