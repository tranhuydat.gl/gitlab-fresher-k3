package com.example.luyentap.controller;

import com.example.luyentap.ProductRespository;
import com.example.luyentap.entity.ProductEntity;
import com.example.luyentap.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000/")
@RestController
@RequestMapping("/api/v1")
public class ProductController {
    @Autowired
    private ProductRespository productRespository;

    // get all product
    @GetMapping("/product")
    public List<ProductEntity> getAllProduct(){
        return productRespository.findAll();
    }

    // create product
    @PostMapping("/product")
    public ProductEntity createProductEntity(@RequestBody ProductEntity productEntity){
        ProductEntity productEntity1 = productEntity;
        return productRespository.save(productEntity);
    }

    // get product by id
    @GetMapping("/product/{id}")
    public ResponseEntity<ProductEntity> getProductEntityById(@PathVariable Long id){
        ProductEntity productEntity = productRespository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("co loi"));
        return ResponseEntity.ok(productEntity);
    }

    // update product
    @PutMapping("/product/{id}")
    public ResponseEntity<ProductEntity> updateEntity(@PathVariable Long id, @RequestBody ProductEntity productEntityDetails){
        ProductEntity productEntity = productRespository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("CO loi"));
        productEntity.setName(productEntityDetails.getName());
        productEntity.setPrice(productEntityDetails.getPrice());
        ProductEntity updateProduct = productRespository.save(productEntity);
        return ResponseEntity.ok(updateProduct);
    }

    // delete product
    @DeleteMapping("/product/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteProduct(@PathVariable Long id){
        ProductEntity productEntity = productRespository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException("khong tim thay"));
        productRespository.delete(productEntity);
        Map<String, Boolean> respon = new HashMap<>();
        respon.put("deleted", Boolean.TRUE);
        return ResponseEntity.ok(respon);
    }

}
