import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class ViDuHashSet {
	public static void main(String[] args) {
		Set<User> setUser = new HashSet<User>();

		User u1 = new User("dongpt", "hn", 33);
		User u2 = new User("dongpt", "hn", 34);
		User u3 = new User("dongpt", "hn", 33);

		setUser.add(u1);
		setUser.add(u2);
		setUser.add(u3);

		System.out.println("u1 = " + u1.hashCode());
		System.out.println("u2 = " + u2.hashCode());
		if (u1.equals(u2) && (u1.hashCode() == u2.hashCode())) {
			System.out.println("true");
		} else {
			System.out.println("false");
		}

		if (u1.getAddress().equals(u2.getAddress()) && (u1.hashCode() == u2.hashCode())) {
			System.out.println("true");
		} else {
			System.out.println("false");
		}

		for (User user : setUser) {
			System.out.println("user = " + user);
		}

	}
}

class User {
	private String name;
	private String address;
	private int age;

	public User(String name, String address, int age) {
		this.name = name;
		this.address = address;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			User user = (User) obj;
//			return name.equals(user.name);
			return age == user.age;
		}
		return false;
//		return super.equals(obj);
	}

	@Override
	public String toString() {
		return "User: Name: " + getName() + ", Address: " + getAddress() + ", Age: " + getAge()
				+ ", hasCode: " + hashCode();
	}

	@Override
	public int hashCode() {
//		return super.hashCode();
		return 1;
	}

}
