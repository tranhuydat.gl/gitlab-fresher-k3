package ThreadAndCollections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparatorDemo {
	public static void main(String[] args) {
		List<Car> listCar = new ArrayList<Car>();

		listCar.add(new Car("Audi", 700, "White"));
		listCar.add(new Car("Honda", 500, "Black"));
		listCar.add(new Car("Toyota", 600, "Red"));

		// sắp xếp theo giá
		Collections.sort(listCar, new Comparator<Car>() {
			@Override
			public int compare(Car car1, Car car2) {
				return car1.getPriceCar() - car2.getPriceCar();
			}
		});

		for (int i = 0; i < listCar.size(); i++) {
			System.out.println(listCar.get(i).getNameCar() + " - " + listCar.get(i).getPriceCar() + " - "
					+ listCar.get(i).getColorCar());
		}
		
		
	}
}

class Car {
	private String nameCar;
	private int priceCar;
	private String colorCar;

	public Car(String nameCar, int priceCar, String colorCar) {
		this.nameCar = nameCar;
		this.priceCar = priceCar;
		this.colorCar = colorCar;
	}

	public String getNameCar() {
		return nameCar;
	}

	public void setNameCar(String nameCar) {
		this.nameCar = nameCar;
	}

	public int getPriceCar() {
		return priceCar;
	}

	public void setPriceCar(int priceCar) {
		this.priceCar = priceCar;
	}

	public String getColorCar() {
		return colorCar;
	}

	public void setColorCar(String colorCar) {
		this.colorCar = colorCar;
	}

}
