package ThreadAndCollections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionsInJava {
	public static void main(String[] args) {
		/*
		 * Lớp Collections có 2 cách sort
		 * - Cách 1: sử dụng đối tượng nặc danh Comparator
		 * - Cách 2: implement từ Comparable interface
		 * 		phải override lại method compareTo
		 */
		
		List<Student> listStudent = new ArrayList<Student>();
		
		listStudent.add(new Student("Nguyễn Văn An", 20));
		listStudent.add(new Student("Nguyễn Văn Anh", 21));
		listStudent.add(new Student("Nguyễn Văn Ánh", 19));
		
		Collections.sort(listStudent);
		for (int i = 0; i < listStudent.size(); i++) {
			System.out.println(listStudent.get(i).getAge()+" - "+listStudent.get(i).getName());
		}
	}
}

class Student implements Comparable<Student>{
	private String name;
	private int age;
	
	public Student(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int compareTo(Student student) {
		// sắp xếp theo tuổi
//		return this.age - student.age;
		
		// sắp xếp theo tên
		return this.getName().compareTo(student.getName());
	}
	
}