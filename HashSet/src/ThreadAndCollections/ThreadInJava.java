package ThreadAndCollections;

public class ThreadInJava {
	public static void main(String[] args) {
		/*
		 * Thread l�? luồng, đơn v�? nh�? nhất trong Java, mỗi thread thực hiện 1 công việc
		 * riêng biệt v�? được quản lý bởi máy ảo JMV, mỗi thread có th�? chạy song song
		 * riêng biệt với nhau  
		 * - có 2 cách tạo thread: 
		 * + kế thừa từ lớp Thread 
		 * + implement từ Runnable
		 */
		/*
		 * =>Vòng đời của thread
		 * New: ban đầu thread được khởi tạo bằng phương thức khởi tạo của lớp thread, nhưng chưa start, thread chưa được cấp phát t�?i nguyên v�? chưa được chạy
		 * Runnable: sau khi gọi method start(), thread được cấp phát t�?i nguyên, chưa chạy
		 * Running:thread �? trạng thái running nếu trình lên lịch của thread đã chọn nó
		 * No-Runnable: trạng thái khi thread vẫn còn sống, nhưng k được chọn đ�? chạy
		 * Terminated: một thread �? trong trạng thái terminated khi phương thức run() của nó b�? thoát
		 *  
		 */
		CachMot cach1 = new CachMot();
		Thread thread2 = new Thread(new CachHai());
		cach1.start();
		thread2.start();
		
	}
}

class CachMot extends Thread {
	@Override
	public void run() {
		for (int i = 1; i <= 1000; i++) {
			System.out.println("[i]c1 = " + i);
		}
	}

}

class CachHai implements Runnable {

	@Override
	public void run() {
		for (int i = 1; i <= 1000; i++) {
			System.out.println("[i]c2 = " + i);
		}
	}

}