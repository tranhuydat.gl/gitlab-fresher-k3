
public class ViDu {
	// biến local
	// biến instance
	// biến static
	public static void main(String[] args) {
		ViDuBienInstance vd1 = new ViDuBienInstance();
		
		
		
	}
	
	public ViDu() {
		super();
		System.out.println("vi du");
	}
	
	
}
class ViDuBienInstance extends ViDu{
	private String bienInstance;

	public String getBienInstance() {
		return bienInstance;
	}

	public void setBienInstance(String bienInstance) {
		this.bienInstance = bienInstance;
	}
	
	class InnerClass{
//		private String bienInstance;
		
		public void demo() {
			System.out.println(bienInstance);
			
		}
	}
	
	// block
	{
		System.out.println("block ");
		System.out.println(bienInstance );
	}
	
	// static
	static {
		System.out.println("block static");
	}

	// constructor
	public ViDuBienInstance() {
		super();
		System.out.println("constructor");
	}
	
}
class bienLocal{
	static String str1;  // pham vi tren cac luong
	
	public void ViDu() {
		String str = "dongpt";
	}
	
}

