package test;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.rikkeisoft.canifashop.base.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tbl_orders")
public class OrderEntity extends BaseEntity {

	@Column(name = "code", length = 45, nullable = false)
	private String code;

	@Column(name = "customer_name", length = 100, nullable = true)
	private String customerName;

	@Column(name = "customer_address", length = 200, nullable = true)
	private String customerAddress;

	@Column(name = "customer_phone", length = 10, nullable = true)
	private char customerPhone;

	@Column(name = "customer_email", length = 100, nullable = true)
	private String customerEmail;

	@Column(name = "note", length = 1000, nullable = true)
	private String note;

	@Column(name = "total", precision = 13, scale = 2, nullable = true)
	private BigDecimal total;
	
	@Column(name = "order_status", nullable = true)
	private int orderStatus; 
	
	@Column(name = "payment_method", nullable = true)
	private int paymentMethod;
	
	@Column(name = "payment_status", nullable = true)
	private boolean paymentStatus = Boolean.FALSE;
}
