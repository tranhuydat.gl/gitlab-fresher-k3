package test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.rikkeisoft.canifashop.base.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "tbl_users")
public class UserEntity extends BaseEntity {
	@Column(name = "name", length = 45, nullable = true)
	private String name;
	
	@Column(name = "avatar", length = 255, nullable = true)
	private String avatar; 
	
	@Column(name = "phone", length = 10, nullable = true)
	private String phone;
	
	@Column(name = "email", length = 255, nullable = false)
	private String email;
	
	@Column(name = "password", length = 255, nullable = false)
	private String password;
}
