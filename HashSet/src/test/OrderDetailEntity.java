package test;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.rikkeisoft.canifashop.base.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tbl_order_details")
public class OrderDetailEntity extends BaseEntity {

	@Column(name = "quantity", nullable = false)
	private int quantity;
}
