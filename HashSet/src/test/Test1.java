package test;

public class Test1 {
	public static void main(String[] args) {
		Bird b1 = new Eagle();
		Bird b2 = new Owl();
		Bird b3 = new Sparrow();
		BirdFactory bf = new BirdFactory(b1);
		
	}
}

interface Bird {
	public void fly();
}

class Owl implements Bird{

	@Override
	public void fly() {
		System.out.println("Owl");
	}
	
}
class Eagle implements Bird{

	@Override
	public void fly() {
		System.out.println("Eagle");
	}
	
}

class Sparrow implements Bird{

	@Override
	public void fly() {
		System.out.println("Sparrow");
	}
	
}

class BirdFactory{
	private Bird bird;

	public BirdFactory(Bird bird) {
		this.bird = bird;
	}

	public Bird getBird() {
		return bird;
	}

	public void setBird(Bird bird) {
		this.bird = bird;
	}
	
}


