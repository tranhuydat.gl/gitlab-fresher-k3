package Buoi7;

public class PhanBiet3Bien {
	// phân biệt phạm vi 3 loại biến local, static, instance
	public static void main(String[] args) {
		Demo1 demo = new Demo1();
		demo.in();
		
		System.out.println(Demo1.bienStatic); // k cần tạo constructor mà gọi luôn biến static
	}

}

class Demo1 {
	/*
	 * biến static => gọi được ở các luồng
	 */
	public static String bienStatic = "dongpt"; // biến static

	/*
	 * biến Instance => nằm trong class, gọi ở trong class, method(khắp nơi trong class mà nó nằm trong)
	 */
	private String bienInstance; // biến instance

	public void in() {
		// biến local:
		/*
		 * => nằm trong method
		 */
		String bienLocal = "dongpt1"; // biến local
		System.out.println("goi biến: " + bienStatic + ", " + bienInstance + ", " + bienLocal);
	}

	class InnerClass {
		public void show() {
			System.out.println("gọi biến: " + bienStatic); // biến static gọi được từ trong inner class
		}
	}
}
