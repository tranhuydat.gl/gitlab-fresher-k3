package Buoi7;

public class BienFunctionStaticTrenClassAndInterface {
	public static void main(String[] args) {
		ViDu.hienThiHamClass();
		TestInterFace.hienThi2();
		System.out.println(ViDu.name + "==== " + TestInterFace.name);
	}

}

class ViDu {
	static String name = "dongpt2";  // biến static trong class gọi đc luôn ở main k cần khởi tạo đt

	static void hienThiHamClass() { // phương thức static trong class gọi đc luôn ở hàm main k cần tạo constructor,
									// phải có block
		System.out.println("method static trong class");
	}
	public void name() {
		System.out.println("test");
	}
}

interface TestInterFace {
	static String name = "dongpt";  // biến static trong interface gọi đc luôn ở main

	static void hienThi2() { // phương thức static trong interface có block, gọi luôn đc ở hàm main k cần tạo
								// constructor.
								// có class nào implement thì nó k ghi đè đc
		System.out.println("method static trong interface");
	};
}

class ExtendsViDu extends ViDu{
	
}

class ExtendsTestInterface implements TestInterFace{
	
}



