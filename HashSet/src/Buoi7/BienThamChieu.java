package Buoi7;

public class BienThamChieu {
	/*
	 * Biến tham chiếu - là giá trị của địa chỉ mà địa chỉ trỏ tới đối tượng ở bộ
	 * nhớ Heap.
	 * => set lại thuộc tính thì thay đổi giá trị đc lưu ở heap còn giá trị địa chỉ thì k thay đổi
	 * => gán đối tượng mới thì thay đổi giá trị địa chỉ, đối tượng truyền vào method k thay đổi địa chỉ
	 */
	int num = 10;

	public BienThamChieu(int num) {
		this.num = num;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	void hienThi(BienThamChieu bienThamChieu) {
		num = bienThamChieu.num + 10;
	}

	public static void main(String[] args) {
		BienThamChieu btc = new BienThamChieu(5);
		BienThamChieu btc2 = new BienThamChieu(20);
		System.out.println(btc +"---");
//		btc.setNum(30);
		btc = btc2;

		System.out.println(btc.num + "----" + btc);
		System.out.println(btc2 + "---");

		btc.hienThi(btc);
		System.out.println(btc.num + "---" + btc);
		
	}

}
