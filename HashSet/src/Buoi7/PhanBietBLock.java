package Buoi7;

public class PhanBietBLock {
	public static void main(String[] args) {
		Test t = new Test();
	}

}

class Test {
	/*
	 * - block static chạy trước
	 * - block instance chạy thứ 2
	 * - constructor chạy thứ 3
	 */
	
	static {
		System.out.println("block static");
	}

	{
		System.out.println("block instance");
	}
	
	public Test() {
		System.out.println("constructor");
	}
}
