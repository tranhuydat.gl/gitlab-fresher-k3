package DesignPattern.CreationalDesign;

public class FactoryMethodPattern {

	public static void main(String[] args) {
		GiongCayTrongFactory mienBac = new MienBac();
		GiongCayTrongFactory mienNam = new MienNam();
	}
}

interface Giong {
	void showTenGiong();
}

class CaPhe implements Giong {

	@Override
	public void showTenGiong() {
		System.out.println("caphe");
	}
}

class CaCao implements Giong {

	@Override
	public void showTenGiong() {
		System.out.println("cacao");
	}
}
class Lua implements Giong{

	@Override
	public void showTenGiong() {
		System.out.println("Lua");
	}
}
class Ngo implements Giong{

	@Override
	public void showTenGiong() {
		System.out.println("Ngo");
	}
}

interface GiongCayTrongFactory{
	public Giong chonGiong();
}

class MienBac implements GiongCayTrongFactory{

	@Override
	public Giong chonGiong() {
		
		return null;
	}
	
}
class MienNam implements GiongCayTrongFactory{

	@Override
	public Giong chonGiong() {
		
		return null;
	}
	
}




























