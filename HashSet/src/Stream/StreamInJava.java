package Stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamInJava {
	public static void main(String[] args) {
		/*
		 * Stream là luồng để thao tác với Collection và array dễ dàng hơn,không phải
		 * cấu trúc để lưu trữ dữ liệu(dữ liệu đi qua nó đc xử lý thông qua các hoạt
		 * động tính toán) không sửa đổi nguồn dữ liệu, chỉ đi qua mỗi phần tử 1 lần
		 * 
		 * Stream trả về 1 stream được xử lý theo tuần tự Parallel Stream trả về 1
		 * stream song song, chia các phần tử của stream thành nhiều khối xử lý giống
		 * nhau trên từng khối sau đó hợp kết quả lại
		 * 
		 */

		List<Integer> listInter = Arrays.asList(2, 4, 3, 5, 6, 8, 7, 9);

		// filter(): là toán tử trung gian, để lọc các phân tử với điều kiện số chẵn
		// count(), collect(): là toán tử hủy.
		long count = listInter.stream().filter(num -> num % 2 == 0).count();
		List<Integer> lstInterLe = listInter.stream().filter(num -> num % 2 != 0).collect(Collectors.toList());

		System.out.println("Số phần tử chẵn: " + count);
		System.out.println(lstInterLe);
	}
}
